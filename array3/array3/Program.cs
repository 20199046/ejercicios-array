﻿using System;

namespace array3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<Binevenido al programa de array>>>>>>>> \n");
            /*Un programa que almacene en un array el número de días que tiene cada mes 
             * (supondremos que es un año no bisiesto),
             * pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.
            */
            int[] meses = new int[12];
            meses[0] = 31;
            meses[1] = 29;
            meses[2] = 31;
            meses[3] = 30;
            meses[4] = 31;
            meses[5] = 30;
            meses[6] = 31;
            meses[7] = 31;
            meses[8] = 30;
            meses[9] = 31;
            meses[10] = 30;
            meses[11] = 31;
            Console.WriteLine("seleccione un mes del 1 al 12 ");
            int clave = int.Parse(Console.ReadLine());
            switch (clave)
            {

                case 1:
                    Console.WriteLine("selecciono enero que tiene {0} dias ", meses[0]);
                    Console.ReadKey();
                    break;

                case 2:
                    Console.WriteLine("selecciono febrero que tiene {0} dias ", meses[1]);
                    Console.ReadKey();
                    break;

                case 3:
                    Console.WriteLine("selecciono marzo tiene {0} dias ", meses[2]);
                    Console.ReadKey();
                    break;

                case 4:
                    Console.WriteLine("selecciono abril que tiene {0} dias ", meses[3]);
                    Console.ReadKey();
                    break;

                case 5:
                    Console.WriteLine("selecciono mayo que tiene {0} dias ", meses[4]);
                    Console.ReadKey();
                    break;

                case 6:
                    Console.WriteLine("selecciono junio que tiene {0} dias ", meses[5]);
                    Console.ReadKey();
                    break;

                case 7:
                    Console.WriteLine("selecciono julio que tiene {0} dias ", meses[6]);
                    Console.ReadKey();
                    break;

                case 8:
                    Console.WriteLine("selecciono agosto que tiene {0} dias ", meses[7]);
                    Console.ReadKey();
                    break;

                case 9:
                    Console.WriteLine("selecciono septiembre que tiene {0} dias ", meses[8]);
                    Console.ReadKey();
                    break;

                case 10:
                    Console.WriteLine("selecciono octubre que tiene {0} dias ", meses[9]);
                    Console.ReadKey();
                    break;

                case 11:
                    Console.WriteLine("selecciono noviembre que tiene {0} dias ", meses[10]);
                    Console.ReadKey();
                    break;

                case 12:
                    Console.WriteLine("selecciono diciembre que tiene {0} dias ", meses[11]);
                    Console.ReadKey();
                    break;
            }
        }
    }
}
