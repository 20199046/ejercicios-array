﻿using System;

namespace array4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<Binevenido al programa de array>>>>>>>> \n");

            //Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.

            int cant = 10, num, mayor = 0, menor = 0;
            Console.WriteLine("Digite los 10 numeros \n");

            for (int i = 0; i < cant; i++)   
            {
                Console.Write("Inicializar el numero");
                num = int.Parse(Console.ReadLine());

                if (i == 0)
                {
                    mayor = num;
                    menor = num;
                }
                else if (i != 0)
                {
                    if (num < menor)
                    {
                        menor = num;
                    }
                    if (num > mayor)
                    {
                        mayor = num;
                    }
                }
            }
            Console.WriteLine("El numero mayor es:" + mayor);
            Console.WriteLine("El numero menor es:" + menor);
            Console.ReadKey();
        }
    }
}
