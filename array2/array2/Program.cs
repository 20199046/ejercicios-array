﻿using System;
using System.Linq;

namespace array2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<Binevenido al programa de array>>>>>>>> \n");
            /*Un programa que pida al usuario 5 números reales 
            (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.
            */

            float[] numeros = new float[5];

            for (int i = 0; i <= 4; i++)   // Pedimos los datos
            {
                Console.Write("Introduce el dato numero {0}: ", i + 1);
                numeros[i] = Convert.ToInt32(Console.ReadLine());
            }
            Array.Reverse(numeros);
            for (int i= 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i].ToString());
            }
            Console.ReadLine();
        }
    }
}
