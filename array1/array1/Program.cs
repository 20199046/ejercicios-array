﻿using System;
using System.Transactions;

namespace array1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<<<<<<<<Binevenido al programa de array>>>>>>>> \n");
            /*Un programa que pida al usuario 4 números, los memorice (utilizando un array),
             * calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.
            */
            int[] num = new int[4];
            double sum = 0;
            double promedio;

            Console.WriteLine("ingrese el primer valor");
            num[0] = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("ingrese el segundo valor");
            num[1] = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("ingrese el tercer valor");
            num[2] = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("ingrese el cuarto valor");
            num[3] = Convert.ToInt32(Console.ReadLine());

          sum = num[0] + num[1] + num[2] +
                num[3];
            promedio = sum / num.Length;
            Console.WriteLine("Los numeros suministrados son: {0}, {1}, {2}, {3}", num[0], num[1], num[2], num[3]);
            Console.WriteLine("la media aritmetica es: {0}", promedio);
            Console.ReadKey();
        }
    }
}
